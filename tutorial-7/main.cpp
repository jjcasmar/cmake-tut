#ifdef COOL_FEATURE
#include <spdlog/spdlog.h>
#else
#include <iostream>
#endif

int main(int, char **)
{
#ifdef COOL_FEATURE
    spdlog::info("This program uses a cool feature");
#else
    std::cout << "This program is boring\n";
#endif
    return 0;
}
