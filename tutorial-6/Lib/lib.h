#ifndef SMALLLIB_LIB_H
#define SMALLLIB_LIB_H

#include <chrono>
#include <spdlog/spdlog.h>

namespace SmallLib
{
void foo();
inline void foo2()
{
#ifdef OUTPUT
    spdlog::info("foo2");
#endif OUTPUT
}
}  // namespace SmallLib

#endif  // SMALLLIB_LIB_H
