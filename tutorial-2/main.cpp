#include <Eigen/Core>
#include <iostream>

int main(int, char **)
{
    Eigen::Vector2d v{1, 1};
    Eigen::Vector2d w{2, 2};
    std::cout << v.dot(w) << "\n";
    return 0;
}
