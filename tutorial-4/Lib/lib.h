#ifndef SMALLLIB_LIB_H
#define SMALLLIB_LIB_H

#include <Eigen/Dense>

namespace SmallLib
{
Eigen::Vector2d norm();
}

#endif  // SMALLLIB_LIB_H
